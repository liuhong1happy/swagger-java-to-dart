package com.neko.seed.base.entity;

import lombok.Getter;

/**
 * 返回结果实体类
 *
 * @author yanghao
 * @update 2019/1/7
 * @since 2018/12/6
 */
@Getter
public class Result<T>  {
    private int code;
    private String message;
    private T data;

    private Result<T> setResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        return this;
    }

    public Result<T> success() {
        return setResult(200, "Success", null);
    }

    public Result<T> success(T data) {
        return setResult(200, "Success", data);
    }

    public Result<T> fail(T data, String message) {
        return setResult(500, message, data);
    }
    public Result<T> fail() {
        return setResult(500, "操作失败", null);
    }

    public Result<T> fail(T data, String message, int code) {
        return setResult(code, message, data);
    }

    public Result<T> fail(String message, int code) {
        return setResult(code, message, null);
    }

    public Result<T> fail(String message) {
        return setResult(500, message, null);
    }
}
