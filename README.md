# Install

```bash
npm install -g swagger-java-to-dart
```

# Quick Install [推荐]

```bash
git clone https://gitee.com/liuhong1happy/swagger-java-to-dart
cd swagger-java-to-dart
npm install
npm link
```

# Use

## 实际用法参考命令工具为准

```bash
swagger-java-to-dart

Usage: swagger-java-to-dart [options] [command]

Options:
  -V, --version          output the version number
  -h, --help             display help for command

Commands:
  fetch [options] <url>  fetch api docs from swagger ui, eg: swagger-java-to-dart fetch
                         http://localhost:8083/v1/v2/api-docs
  gen [options]          gen dart code from api docs, eg: gen -c ./config.js -o ./lib/controllers
  help [command]         display help for command
```

## 后端选用脚手架推荐

需要后端脚手架工具 `springboot-restful-starter`

如果Result需要返回具体结果类，则需要修改Result为范式。

注：参考[Result.java](./java/Result.java)。

极为推荐使用 `Result<Order> `之类的用法来返回结果。

## 后端注意事项

1. 定义数据库表对应实体类时，不能使用`@ApiModelvalue`注解.
