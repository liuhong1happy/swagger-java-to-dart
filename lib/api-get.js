var child_process = require("child_process");
var iconv = require('iconv-lite');
var encoding = 'cp936';
var path = require('path')

exports.getApiDoc = function getApiDoc(apiDocUrl, doc) {
    var filePath = path.resolve(process.cwd(), doc)
    var curl = `curl "${apiDocUrl}" > "${filePath}"`
    var child = child_process.exec(curl, { encoding: 'buffer' }, function(err, stdout, stderr) {
        console.log(
            iconv.decode(stderr, encoding),
            iconv.decode(stdout, encoding)
        );
    });
}
