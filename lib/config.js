var DEFINITION_FILE_EXT = 'dart';

var DEFINITION_INTERFACE_DEFINE = `
abstract class Definition {
  Map<String, dynamic> toJson();
  void fromJson(Map<String, dynamic> json);
}
`;

var RESULT_INTEFACE_DEFINE = `
import 'Definition.dart';

abstract class Result<T extends Definition> {
  int code;
  String message;
  T data;
  @override
  Map<String, dynamic> toJson() {
    return {
      "data": data.toJson(),
      "code": code,
      "message": message
    };
  }

  T fromDataJson(Map<String, dynamic> json);

  @override
  fromJson(Map<String, dynamic> json) {
    data = json["data"] is! Map ? json["data"] : fromDataJson(json["data"]);
    code = json["code"];
    message = json["message"];
  }
}

abstract class ResultList<T extends Definition> {
  int code;
  String message;
  List<T> data;

  Map<String, dynamic> toJson() {
    return {
      "data": data?.map((e) => e.toJson())?.toList(),
      "code": code,
      "message": message
    };
  }

  T fromDataJson(Map<String, dynamic> json);

  fromJson(Map<String, dynamic> json) {
    data = (json["data"] as List)?.map<T>((e) => fromDataJson(e))?.toList();
    code = json["code"];
    message = json["message"];
  }
}
`


function definitionClassTmpl(CLASS_NAME, CLASS_REFS, PROPERTIES) {
  return `
import 'Definition.dart';
${CLASS_REFS}
class ${CLASS_NAME} extends Definition {
  ${PROPERTIES}
}
  `;
}

function definitionClassRefsTmpl(REFS) {
  return REFS.map(ref=> {
    if(!ref) return "";
    return `import '${ref}.dart';\n`
  }).join("");
}

function definitionPropertiesTmpl(definition) {
  return `
${
  (()=> {
    return Object.keys(definition.properties).map((propertyName)=> {
      var property = definition.properties[propertyName];
      if(property.$ref) {
        var className = definition.findRef(property.$ref).className;
        return `\t${className} ${propertyName};\n`;
      }
      var desc = property.description ? `\t/// ${property.description}\n` : "";
      if(property.enum) {
        desc += `\t/// 枚举值: ${property.enum.toString()}\n`;
      }
      switch (property.type) {
        case 'string':
          return `${desc}\tString ${propertyName};\n`;
        case 'integer':
          return `${desc}\tint ${propertyName};\n`;
        case 'float':
        case 'number':
          return `${desc}\tnum ${propertyName};\n`;
        case 'array':
          if(property.items.$ref) {
            console.log(property.items.$ref, definition.findRef(property.items.$ref))
          }
          var className = property.items.$ref ?
            definition.findRef(property.items.$ref).className :
            'dynamic'
          ;
          return `${desc}\tList<${className}> ${propertyName};\n`;
        default:
          return `${desc}\tdynamic ${propertyName};\n`;
      }
    }).join("")
  })()
}
  @override
  Map<String, dynamic> toJson() {
    return {
${
  (()=> {
    return Object.keys(definition.properties).map((propertyName)=> {
      var property = definition.properties[propertyName];
      if(property.$ref) {
        // var className = definition.findRef(property.$ref).className;
        return `\t\t\t"${propertyName}": ${propertyName}?.toJson(),\n`;
      } 
      if(property.type === "array") {
        return `\t\t\t"${propertyName}": ${propertyName}?.map((e) => e.toJson())?.toList(),\n`
      } else {
        return `\t\t\t"${propertyName}": ${propertyName},\n`
      }
    }).join("")
  })()
}
    };
  }

  @override
  void fromJson(Map<String, dynamic> json) {
${
  (()=> {
    return Object.keys(definition.properties).map((propertyName)=> {
      var property = definition.properties[propertyName];
      if(property.$ref) {
        var className = definition.findRef(property.$ref).className;
        return `\t\t${propertyName} = json["${propertyName}"] == null ? null : (${className}()..fromJson(json["${propertyName}"]));\n`;
      } 
  
      if(property.type === "array") {
        if(property.items.$ref) {
          var className = definition.findRef(property.items.$ref).className;
          return `\t\t\t${propertyName} = (json["${propertyName}"] as List)?.map<${className}>((e) => ${className}()..fromJson(e))?.toList();\n`
        } else {
          var className = 'dynamic'
          switch (property.items.type) {
            case 'string':
                className = 'String';
              break;
              case 'integer':
                className = 'int';
                break;
              case 'float':
              case 'number':
                className = 'num';
                break;
            default:
              break;
          }
          return `\t\t\t${propertyName} = (json["${propertyName}"] as List)?.map<${className}>((e) => e as ${className})?.toList();\n`
        ;
        }
      } else {
        return `\t\t${propertyName} = json["${propertyName}"];\n`;
      }
    }).join("")
  })()
}
  }
  `;
}



function classTmpl(ClassName, ClassMethods, REFS){
  return `
import 'dart:async';
import 'package:chechen/api/api_service.dart';
${REFS}

class ${ClassName} {
${ClassMethods}
}`
} 
  
function methodTmpl(api){
  var OperationId = api.operationId;
  var Summary = api.summary;
  var Path = api.path;
  var Method = api.method;

  var paramsRegex = /(\{\w+\})/gm;
  var params = Path.match(paramsRegex) || [];
  var _url = Path;
  var paramsArgs = [];
  for(var i=0;i<params.length;i++) {
    var param = params[i].replace(/[\{\}]/g, "");
    _url = _url.replace(params[i], "$"+param);
    paramsArgs.push(`String ${param}, `);
  }

  var queryArgs = [];
  var queryStrArray = [];
  if(api.queryParams) {
    for(var i=0;i< api.queryParams.length;i++) {
      var param = api.queryParams[i];
      var paramName = param.name;
      queryStrArray.push(`${paramName}=\$\{${paramName} ?? "" \}`);
      switch (param.format) {
        case 'string':
          queryArgs.push(`String ${paramName}, `);
          break;
        case 'integer':
          queryArgs.push(`int ${paramName}, `);
          break;
        case 'float':
          queryArgs.push(`num ${paramName}, `);
          break;
        default:
          queryArgs.push(`dynamic ${paramName}, `);
          break;
      }
    }
    if(queryStrArray.length>0) {
      _url += "?" + queryStrArray.join("&");
    }
  }
  
  if(Method == "post") {
    var ResponseParam = api.parseResponse && api.parseResponse() || "dynamic";
    var ParseBodyParam = api.parseBodyParam && api.parseBodyParam();
    return `
    static String ${OperationId}Path = "${Path}";
    /// ${Summary}
    Future<${ResponseParam}> ${OperationId}(${paramsArgs.join("")}${queryArgs.join("")}${ParseBodyParam || ""}[ dynamic headers ]) {
      Completer<${ResponseParam}> completer = Completer<${ResponseParam}>();
      ApiService.post(
        ApiService.url("${_url}"), 
        ${!ParseBodyParam ? "null," : "data.toJson(), "}
        headers ?? ApiService.getHeader(),
        success: (json) {
          completer.complete(
            ${
              ResponseParam != "dynamic" ? `${ResponseParam}()..fromJson(json)` : "json"
            }
          );
        },
        fail: (error) {
          completer.completeError(error);
        }
      );
      return completer.future;
    }
    `
  }

  if(Method == "put") {
    var ResponseParam = api.parseResponse && api.parseResponse() || "dynamic";
    var ParseBodyParam = api.parseBodyParam && api.parseBodyParam();
    return `
    static String ${OperationId}Path = "${Path}";
    /// ${Summary}
    Future<${ResponseParam}> ${OperationId}(${paramsArgs.join("")}${queryArgs.join("")}${ParseBodyParam || ""}[ dynamic headers ]) {
      Completer<${ResponseParam}> completer = Completer<${ResponseParam}>();
      ApiService.putData(
        ApiService.url("${_url}"), 
        ${!ParseBodyParam ? "null," : "data.toJson(), "}
        success: (json) {
          completer.complete(
            ${
              ResponseParam != "dynamic" ? `${ResponseParam}()..fromJson(json)` : "json"
            }
          );
        },
        fail: (error) {
          completer.completeError(error);
        }
      );
      return completer.future;
    }
    `
  }

  if(Method == "get") {
    var ResponseParam = api.parseResponse && api.parseResponse() || "dynamic";
    return `
    static String ${OperationId}Path = "${Path}";

    Future<${ResponseParam}> ${OperationId}(${paramsArgs.join("")}${queryArgs.join("")}[dynamic headers]) {
      Completer<${ResponseParam}> completer = Completer<${ResponseParam}>();
      ApiService.getData(
        ApiService.url("${_url}"),
        success: (json) {
          completer.complete(
            ${
              ResponseParam != "dynamic" ? `${ResponseParam}()..fromJson(json)` : "json"
            }
          );
        },
        fail: (error) {
          completer.completeError(error);
        }
      );
      return completer.future;
    }
    `
  }

  if(Method == "delete") {
    var ResponseParam = api.parseResponse && api.parseResponse() || "dynamic";
    return `
    static String ${OperationId}Path = "${Path}";

    Future<${ResponseParam}> ${OperationId}(${paramsArgs.join("")}${queryArgs.join("")}[dynamic headers]) {
      Completer<${ResponseParam}> completer = Completer<${ResponseParam}>();
      ApiService.delete(
        ApiService.url("${_url}"),
        success: (json) {
          completer.complete(
            ${
              ResponseParam != "dynamic" ? `${ResponseParam}()..fromJson(json)` : "json"
            }
          );
        },
        fail: (error) {
          completer.completeError(error);
        }
      );
      return completer.future;
    }
    `
  }

  return "";
}


function resultClassTmpl(className, extendsClassName, refClassName) {
  var originRefClassName;
  if(extendsClassName.includes('Result<List')) {
    extendsClassName = extendsClassName.replace('Result<List', 'ResultList<')
    var matches = /^ResultList\<(.+)\>$/g.exec(extendsClassName);
    originRefClassName = matches[1];
  } else {
    var matches = /^Result\<(.+)\>$/g.exec(extendsClassName);
    originRefClassName = matches[1];
  }
  

  return `
import 'Result.dart';
import '${refClassName}.dart';

class ${className} extends ${extendsClassName.replace(originRefClassName, refClassName)} {
  @override
  ${refClassName} fromDataJson(Map<String, dynamic> json) {
    return ${refClassName}()..fromJson(json);
  }
}
`
}

module.exports = {
  DEFINITION_FILE_EXT: DEFINITION_FILE_EXT,
  DEFINITION_INTERFACE_DEFINE: DEFINITION_INTERFACE_DEFINE,
  RESULT_INTEFACE_DEFINE: RESULT_INTEFACE_DEFINE,
  definitionClassTmpl: definitionClassTmpl,
  definitionPropertiesTmpl: definitionPropertiesTmpl,
  definitionClassRefsTmpl: definitionClassRefsTmpl,
  resultClassTmpl: resultClassTmpl,
  shouldGenDefinition: false,
  classTmpl: classTmpl,
  methodTmpl: methodTmpl,
}