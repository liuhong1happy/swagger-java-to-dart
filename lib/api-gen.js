const { genApiControllers } = require('./helpers/Api');
const { genDefinitions } = require('./helpers/Definition');
const { delDir, mkdirSync } = require('./helpers/FileHelper');
const { genMergeConfig, getMergeConfig } = require('./helpers/mergeConfig');

exports.genApiCode = function(args) {
  /// 1. 获取配置
  genMergeConfig(args)
  var mergeConfig = getMergeConfig()
  /// 删除原有文件夹
  delDir(mergeConfig.DIR_CONTROLLERS)
  /// 重新创建文件夹
  mkdirSync(mergeConfig.DIR_CONTROLLERS)
  /// 2. 生成实体定义文件(可选)
  if(mergeConfig.shouldGenDefinition) {
    genDefinitions()
  }
  /// 3. 生成接口定义文件
  genApiControllers()
}
