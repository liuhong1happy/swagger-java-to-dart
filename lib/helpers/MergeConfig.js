var defaultConfig = require('../config');
var path = require('path');

function MergeConfig(config) {
  this.DEFINITION_FILE_EXT = config.DEFINITION_FILE_EXT;
  this.DEFINITION_INTERFACE_DEFINE = config.DEFINITION_INTERFACE_DEFINE;
  this.RESULT_INTEFACE_DEFINE = config.RESULT_INTEFACE_DEFINE;
  this.definitionClassTmpl = config.definitionClassTmpl;
  this.definitionPropertiesTmpl = config.definitionPropertiesTmpl;
  this.definitionClassRefsTmpl = config.definitionClassRefsTmpl;
  this.resultClassTmpl = config.resultClassTmpl;
  this.shouldGenDefinition = config.shouldGenDefinition;
  this.classTmpl = config.classTmpl;
  this.methodTmpl = config.methodTmpl;
  this.apiDoc = config.apiDoc;
  this.DIR_CONTROLLERS = config.DIR_CONTROLLERS;
  this.DIR_DEFINITIONS = config.DIR_DEFINITIONS;
  this.nameToCaml = config.nameToCaml;
  this.host = config.host
}

MergeConfig.prototype.DEFINITION_FILE_EXT = null;
MergeConfig.prototype.DEFINITION_INTERFACE_DEFINE = null;
MergeConfig.prototype.RESULT_INTEFACE_DEFINE = null;
MergeConfig.prototype.definitionClassTmpl = function(className, classRefs, props) {
  return "";
}

MergeConfig.prototype.definitionPropertiesTmpl = function(definition) {
  return "";
}

MergeConfig.prototype.definitionClassRefsTmpl = function(refs) {
  return "";
}

MergeConfig.prototype.resultClassTmpl = function(className, extendsClassName, refClassName) {
  return "";
}

MergeConfig.prototype.shouldGenDefinition = false;

MergeConfig.prototype.classTmpl = function(className, classMethods, refs){
  return "";
} 

MergeConfig.prototype.methodTmpl = function(api) {
  return "";
}

MergeConfig.prototype.apiDoc = null;

MergeConfig.prototype.DIR_CONTROLLERS = null;
MergeConfig.prototype.DIR_DEFINITIONS = null;

MergeConfig.prototype.nameToCaml = function(name) {
  return name;
}


module.exports = MergeConfig

var mergeConfig = {}

module.exports.genMergeConfig = function genMergeConfig(args) {
  var config = args.config ? require(path.resolve(process.cwd(), args.config)) : {};
  var apiDoc = require(path.resolve(process.cwd(), args.doc || './api-doc.json'));
  var host = args.host || ""
  var _mergeConfig = {
    ...defaultConfig, 
    ...config,
    host: host,
    apiDoc: apiDoc,
    DIR_CONTROLLERS: path.resolve(process.cwd() , args.output),
    DIR_DEFINITIONS: path.resolve(process.cwd() , args.output , "definitions")
  };
  mergeConfig = new MergeConfig(_mergeConfig)
}
module.exports.getMergeConfig = function() {
  return mergeConfig;
}