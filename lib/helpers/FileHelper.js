var fs = require('fs');

module.exports.delDir = function delDir(p){
  if(!fs.existsSync(p)) return;
  var arr = fs.readdirSync(p);
  for(var i in arr){
    //读取文件信息，以便于判断是否是一个文件或目录
    var stats=fs.statSync(p+'/'+arr[i]);
    if(stats.isFile()){
      //判断为真，是文件则执行删除文件
      fs.unlinkSync(p+'/'+arr[i]);
    }else{
      //判断为假就是文件夹，就调用自己，递归的入口
      delDir(p+'/'+arr[i]);
    }
  }
  //删除空目录
  fs.rmdirSync(p, {
    recursive: true
  });
}

module.exports.writeFileSync = function writeFileSync(path, content) {
  fs.writeFileSync(path, content, {
    encoding: "utf-8"
  })
}

module.exports.mkdirSync = function mkdirSync(dir) {
  if(!fs.existsSync(dir)) fs.mkdirSync(dir, {
    recursive: true
  })
}
