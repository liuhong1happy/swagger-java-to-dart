const { getMergeConfig } = require("./mergeConfig")

module.exports.nameToCaml = function nameToCaml (str) {
  if(!str || str === "") {
    return null
  }
  var mergeConfig = getMergeConfig()
  if(mergeConfig.nameToCaml) {
    str = mergeConfig.nameToCaml(str)
  }
  while (str.indexOf('_') != -1) {
    let index = str.indexOf('_')
    str = str.slice(0,index) + str.slice(index+1,index+2).toLocaleUpperCase() + str.slice(index+2,)
  }
  while (str.indexOf('-') != -1) {
    let index = str.indexOf('-')
    str = str.slice(0,index) + str.slice(index+1,index+2).toLocaleUpperCase() + str.slice(index+2,)
  }
  if (str.includes('«')) {
    str = str.replace(/«/gi, "")
  }
  if (str.includes('»')) {
    str = str.replace(/»/gi, "")
  }
  if (str.includes(',')) {
    str = str.replace(/,/gi, "")
  }
  str = str[0].toUpperCase() + str.slice(1, str.length)
  return str;
}

module.exports.nameToLowerCaseCaml = function (str) {
  str = str.replace(/\_\d+$/, '')
  while (str.indexOf('_') != -1) {
    let index = str.indexOf('_')
    str = str.slice(0,index) + str.slice(index+1,index+2).toLocaleUpperCase() + str.slice(index+2,)
  }
  while (str.indexOf('-') != -1) {
    let index = str.indexOf('-')
    str = str.slice(0,index) + str.slice(index+1,index+2).toLocaleUpperCase() + str.slice(index+2,)
  }
  str = str[0].toLowerCase() + str.slice(1, str.length)
  return str;
}

var refMap = {};

module.exports.findRef = function findRef(refName) {
  return refMap[refName];
}

module.exports.refMap = refMap;
