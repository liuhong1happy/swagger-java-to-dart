#!/usr/bin/env node

var program = require('commander');
const { getApiDoc } = require('./api-get');
const { genApiCode } = require('./api-gen');

var fetchCommand = program
  .createCommand('fetch')
  .arguments('<url>')
  .option('-d, --doc <doc>', 'output api doc json file', './api-doc.json')
  .description('fetch api docs from swagger ui, eg: swagger-java-to-dart fetch http://localhost:8083/v1/v2/api-docs')
  .action((args, options, command) => {
    var url = command.args[0];

    getApiDoc(url, options.doc);
  })

var genCommand = program
  .createCommand('gen')
  .description('gen dart code from api docs, eg: gen -c ./config.js -o ./lib/controllers')
  .option('-c, --config <config>', 'javascript config file')
  .option('-o, --output <output>', 'output dir to gen code', './lib/controllers')
  .option('-d, --doc <doc>', 'api doc json file', './api-doc.json')
  .option('-h, --host <host>', 'api host', '')
  .action((options) => {
    genApiCode(options)
  })


program
  .version("1.0.0")
  .addCommand(fetchCommand)
  .addCommand(genCommand)
  .parse(process.argv);