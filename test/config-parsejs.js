/* eslint-disable */
var DEFINITION_FILE_EXT = 'js'

function classTmpl(className, methods, refs) {
  return `/* eslint-disable */
import request from '@yhcsys/antdv-pro-core/es/utils/request'
${refs}
export class ${className} {
  ${methods}
}`
}

function methodTmpl(api) {
  var OperationId = api.operationId
  var Summary = api.summary
  var Path = api.path
  var Method = api.method
  var BaseUrl = api.basePath
  var host = api.host
  var UpperMethod = Method.toUpperCase()

  var paramsRegex = /(\{\w+\})/gm
  var params = Path.match(paramsRegex) || []
  var _url = BaseUrl + Path
  var pathUrl = BaseUrl + Path
  var paramsArgs = []
  for (var i = 0; i < params.length; i++) {
    var pathParam = params[i].replace(/[\{\}]/g, '')
    _url = _url.replace(params[i], '${' + pathParam + '}')
    paramsArgs.push(`${pathParam}, `)
  }

  var queryArgs = []
  var queryStrArray = []
  if (api.queryParams) {
    for (var j = 0; j < api.queryParams.length; j++) {
      var param = api.queryParams[j]
      var paramName = param.name
      queryStrArray.push(`${paramName}=\$\{${paramName} || '' \}`)
      queryArgs.push(`${paramName}, `)
    }
    if (queryStrArray.length > 0) {
      _url += '?' + queryStrArray.join('&')
    }
  }

  if (Method === 'post' || Method === 'put') {
    var ParseBodyParam = api.parseBodyParamJs && api.parseBodyParamJs()
    return `
  static ${OperationId}Path = '${pathUrl}'
  /** ${Summary} */
  static async ${OperationId} (${paramsArgs.join('')}${queryArgs.join('')}${ParseBodyParam || ''}headers) {
    return request(\`${_url}\`, {
      baseURL: ${host},
      method: '${UpperMethod}',
      data: ${!ParseBodyParam ? 'null' : 'JSON.stringify(data)'},
      headers: headers
    })
  }`
  }
  if (Method === 'get' || Method === 'delete') {
    return `
  static ${OperationId}Path = '${pathUrl}'
  /** ${Summary} */
  static async ${OperationId} (${paramsArgs.join('')}${queryArgs.join('')}headers) {
    return request(\`${_url}\`, {
      baseURL: ${host},
      method: '${UpperMethod}',
      headers: headers
    })
  }`
  }

  return ''
}

module.exports = {
  DEFINITION_FILE_EXT: DEFINITION_FILE_EXT,
  shouldGenDefinition: false,
  classTmpl: classTmpl,
  methodTmpl: methodTmpl,
  nameToCaml: str => {
    // if (Object.keys(tagsMap).includes(str)) {
    //   str = tagsMap[str]
    // }
    return str
  }
}
