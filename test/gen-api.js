const { genApiCode } = require("../lib/api-gen");

function main() {
  genApiCode({
    doc: './test/api-doc.json',
    config: './test/config-parsedart.js',
    output: './test/output',
    host: ''
  })
}

main()